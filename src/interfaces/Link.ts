export interface CreateLink {
  amount: number;
  description: string;
  currency?: "USD" | "PYG";
}
