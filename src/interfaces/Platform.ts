export default interface Platform {
  id: number;
  name: string;
  fee: number;
  feeIfExempt: number;
  logo: string;
  type: string;
  createAt: string;
  updateAt: string;
  deleteAt: null | string;
}
