export interface CreateTransaction {
  amount: number;
  description: string;
  reference: string;
  withInvoice?: boolean;
  currency?: "PYG" | "USD";
  platformId: number;
  customer: {
    phone: string;
    email: string;
    name: string;
    identification: string;
  };
}

export interface TransactionReference {
  reference: number;
  redirectUrl: string;
}

export interface Transaction {}
