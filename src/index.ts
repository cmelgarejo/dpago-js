import Links from "./services/Links";
import Platforms from "./services/Platforms";
import Transactions from "./services/Transactions";

export default class Dpago {
  public platforms: Platforms;
  public transactions: Transactions;
  public links: Links;

  constructor(commerceId: number, publicToken: string) {
    this.platforms = new Platforms(commerceId);
    this.transactions = new Transactions(commerceId, publicToken);
    this.links = new Links(commerceId);
  }
}
