import axios from "axios";
import Platform from "../interfaces/Platform";
import { API_URL } from "../config";

export default class Platforms {
  constructor(private commerceId: number) {}

  /**
   * @description Obtiene las plataformas disponibles para tu comercio
   * @returns {Promise<Platforms[]>} Plataformas aceptaas por tu comercio
   */
  public async getAllPlatforms(): Promise<Platform[]> {
    try {
      const { data: platforms } = await axios.get<Platform[]>(
        `${API_URL}/commerces/${this.commerceId}/platforms`
      );

      return platforms;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
