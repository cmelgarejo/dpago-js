import axios from "axios";
import {
  CreateTransaction,
  TransactionReference,
} from "../interfaces/Transaction";
import * as cryptoJs from "crypto-js";
import { API_URL } from "../config";

export default class Transactions {
  constructor(private commerceId: number, private publicToken: string) {}

  /**
   * @description Crea una transacción
   * @param {CreateTransaction} payload Datos de la transacción
   * @returns {Promise<TransactionReference>} Referencia de la transacción
   */
  public async create(
    payload: CreateTransaction
  ): Promise<TransactionReference> {
    try {
      const { withInvoice = false, currency = "PYG" } = payload;
      const token = cryptoJs
        .SHA256(payload.reference + String(payload.amount) + this.publicToken)
        .toString();
      const payloadSend = {
        ...payload,
        token,
        type: "common",
        withInvoice,
        currency,
        commerceId: this.commerceId,
      };

      const { data: dpReference } = await axios.post<TransactionReference>(
        `${API_URL}/transactions`,
        payloadSend
      );

      return dpReference;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
