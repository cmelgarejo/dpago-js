import axios from "axios";
import { CreateLink } from "../interfaces/Link";
import { API_URL } from "../config";

export default class Links {
  constructor(private commerceId: number) {}

  /**
   * @description Crea un link de pago
   * @param {CreateLink} payload Datos del link
   * @returns {string} Link generado
   */
  public async create(payload: CreateLink): Promise<string> {
    try {
      const { currency = "PYG" } = payload;

      const payloadSend = {
        ...payload,
        type: "link",
        currency,
        commerceId: this.commerceId,
      };
      const { data: link } = await axios.post(`${API_URL}/links`, payloadSend);

      const baseLink = "https://pago.dpago.com";

      return baseLink + `/link/${link.reference}`;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
