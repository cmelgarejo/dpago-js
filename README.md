# Dpago

Bienvenido/a a la Dpago! Esta librería fue creada para facilitar los pagos a través de la plataforma Dpago, ofreciendo una interfaz sencilla y conveniente para interactuar con sus servicios de pago.

## Descripción

Dpago es una plataforma de pagos en línea que permite a los comercios procesar transacciones de manera segura y confiable. Esta librería proporciona una serie de funciones y métodos para interactuar con la API de Dpago y realizar operaciones como crear transacciones, links de pago y más.

## Documentación

Para obtener información detallada sobre cómo utilizar la Dpago, consulta nuestra documentación oficial en [https://docs.dpago.com](https://docs.dpago.com). La documentación proporciona ejemplos, guías y detalles sobre cada función disponible en la librería, lo que te permitirá integrar fácilmente la plataforma Dpago en tus proyectos.

## Instalación

Para utilizar esta librería en tu proyecto, simplemente ejecuta el siguiente comando:

```bash
npm install dpago-js
```

o

```bash
yarn add dpago-js
```

## Contribuciones

Si encuentras algún problema o tienes alguna sugerencia de mejora, ¡estamos abiertos a recibir contribuciones! Siéntete libre de abrir un "issue" o enviar un "merge request" en nuestro repositorio en GitLab.

## Licencia

Esta librería está licenciada bajo la Licencia MIT.

¡Gracias por utilizar la Dpago y esperamos que te sea de gran ayuda para tus proyectos de pago en línea!
