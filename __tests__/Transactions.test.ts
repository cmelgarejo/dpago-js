// transactions.test.ts
import axios from "axios";
import Transactions from "../src/services/Transactions";
import * as cryptoJs from "crypto-js";
import { API_URL } from "../src/config";

jest.mock("axios");

jest.mock("crypto-js", () => ({
  SHA256: jest.fn(),
}));

describe("Transactions", () => {
  const commerceId = 123;
  const publicToken = "public_token";
  const reference = "transaction_reference";
  const amount = 1000;
  // @ts-ignore
  const createTransaction: CreateTransaction = {
    reference,
    amount,
    // Otros campos necesarios para la prueba
  };

  beforeEach(() => {
    // Restaurar el comportamiento original de axios antes de cada prueba
    jest.restoreAllMocks();
  });

  const simulatedToken = "simulated_token";
  test("create should return a transaction reference", async () => {
    // Datos de prueba
    const dpReference = "dp_transaction_reference";

    // Configurar el mock de axios.post para resolver con el dpReference simulado
    (
      axios.post as jest.MockedFunction<typeof axios.post>
    ).mockResolvedValueOnce({
      data: dpReference,
    });

    // Configurar el mock de cryptoJs.SHA256 para devolver el valor simulado
    (cryptoJs.SHA256 as jest.Mock).mockReturnValue({
      toString: () => simulatedToken,
    });

    // Crear una instancia de la clase Transactions para probar la función
    const transactions = new Transactions(commerceId, publicToken);

    // Llamar a la función create y esperar la respuesta
    const result = await transactions.create(createTransaction);

    // Verificar que la función axios.post haya sido llamada con los parámetros correctos
    expect(axios.post).toHaveBeenCalledWith(`${API_URL}/transactions`, {
      ...createTransaction,
      token: simulatedToken,
      type: "common",
      withInvoice: false,
      currency: "PYG",
      commerceId,
    });

    // Verificar que la función create retorne el dpReference esperado
    expect(result).toBe(dpReference);
  });

  // Puedes agregar más pruebas aquí

  // Por ejemplo:
  test("create should reject with an error if the request fails", async () => {
    // Configurar el mock de axios.post para simular un error en la solicitud
    const error = new Error("Request failed");
    (
      axios.post as jest.MockedFunction<typeof axios.post>
    ).mockRejectedValueOnce(error);

    // Crear una instancia de la clase Transactions para probar la función
    const transactions = new Transactions(commerceId, publicToken);

    // Llamar a la función create y esperar el rechazo de la promesa
    await expect(transactions.create(createTransaction)).rejects.toThrowError(
      error
    );

    // Verificar que la función axios.post haya sido llamada con los parámetros correctos
    expect(axios.post).toHaveBeenCalledWith(`${API_URL}/transactions`, {
      ...createTransaction,
      token: simulatedToken,
      type: "common",
      withInvoice: false,
      currency: "PYG",
      commerceId,
    });
  });
});
