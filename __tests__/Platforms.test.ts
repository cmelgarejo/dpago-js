// __tests__/Platforms.test.ts
import axios from "axios";
import Platforms from "../src/services/Platforms";
import { API_URL } from "../src/config";

jest.mock("axios");

describe("Platforms", () => {
  const commerceId = 2;

  beforeEach(() => {
    // Restaurar el comportamiento original de axios antes de cada prueba
    jest.restoreAllMocks();
  });

  test("getAllPlatforms should return an array of platforms", async () => {
    // Datos de prueba
    const platformData = [
      {
        id: 1,
        name: "tigo",
        fee: 0.055,
        feeIfExempt: 0.03,
        logo: "https://files.dpago.com/static/platforms/6e74f1248621d666cd094000a130ff15.svg",
        type: "Billetera",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 2,
        name: "personal",
        fee: 0.055,
        feeIfExempt: 0.02,
        logo: "https://files.dpago.com/static/platforms/e74f1248621d666cd094000a130ff158.svg",
        type: "Billetera",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 3,
        name: "wally",
        fee: 0.055,
        feeIfExempt: 0.03,
        logo: "https://files.dpago.com/static/platforms/74f1248621d666cd094000a130ff1587.svg",
        type: "Billetera",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 4,
        name: "zimple",
        fee: 0.055,
        feeIfExempt: 0.03,
        logo: "https://files.dpago.com/static/platforms/4f1248621d666cd094000a130ff15875.png",
        type: "Billetera",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 5,
        name: "tarjeta de crédito",
        fee: 0.055,
        feeIfExempt: 0.052,
        logo: "https://files.dpago.com/static/platforms/0c7b46ea6e74f1248621d666cd094000.png",
        type: "Nacional",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 6,
        name: "qr",
        fee: 0.055,
        feeIfExempt: 0.052,
        logo: "https://files.dpago.com/static/platforms/b46ea6e74f1248621d666cd094000a13.png",
        type: "Nacional",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 7,
        name: "transferencia bancaria",
        fee: 0,
        feeIfExempt: 0,
        logo: "https://files.dpago.com/static/platforms/a6e74f1248621d666cd094000a130ff1.png",
        type: "Nacional",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 8,
        name: "infonet",
        fee: 0.055,
        feeIfExempt: 0.055,
        logo: "https://files.dpago.com/static/platforms/46ea6e74f1248621d666cd094000a130.png",
        type: "Boca de cobranza",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 9,
        name: "pago express",
        fee: 0.055,
        feeIfExempt: 0.055,
        logo: "https://files.dpago.com/static/platforms/6ea6e74f1248621d666cd094000a130f.svg",
        type: "Boca de cobranza",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 10,
        name: "wepa",
        fee: 0.055,
        feeIfExempt: 0.055,
        logo: "https://files.dpago.com/static/platforms/ea6e74f1248621d666cd094000a130ff.png",
        type: "Boca de cobranza",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 11,
        name: "paypal",
        fee: 0.1,
        feeIfExempt: 0.09,
        logo: "https://files.dpago.com/static/platforms/c7b46ea6e74f1248621d666cd094000a.png",
        type: "Internacional",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
      {
        id: 12,
        name: "tarjeta de crédito internacional",
        fee: 0.1,
        feeIfExempt: 0.09,
        logo: "https://files.dpago.com/static/platforms/7b46ea6e74f1248621d666cd094000a1.png",
        type: "Internacional",
        createAt: "2023-07-27T17:33:58.580Z",
        updateAt: "2023-07-27T17:33:58.580Z",
        deleteAt: null,
      },
    ];
    const expectedResponse = platformData;

    // Mockear la función get de axios para que retorne los datos de prueba
    (axios.get as jest.MockedFunction<typeof axios.get>).mockResolvedValueOnce({
      data: platformData,
    });

    // Crear una instancia de la clase Platforms para probar la función
    const platforms = new Platforms(commerceId);

    // Llamar a la función getAllPlatforms y esperar la respuesta
    const result = await platforms.getAllPlatforms();

    // Verificar que la función axios.get haya sido llamada con los parámetros correctos
    expect(axios.get).toHaveBeenCalledWith(
      `${API_URL}/commerces/${commerceId}/platforms`
    );

    // Verificar que la función getAllPlatforms retorne los datos esperados
    expect(result).toEqual(expectedResponse);
  });

  test("getAllPlatforms should reject with an error if the request fails", async () => {
    // Mockear la función get de axios para que retorne un error
    (axios.get as jest.MockedFunction<typeof axios.get>).mockRejectedValueOnce(
      new Error("Request failed")
    );

    // Crear una instancia de la clase Platforms para probar la función
    const platforms = new Platforms(commerceId);

    // Llamar a la función getAllPlatforms y esperar el rechazo de la promesa
    await expect(platforms.getAllPlatforms()).rejects.toThrowError(
      "Request failed"
    );

    // Verificar que la función axios.get haya sido llamada con los parámetros correctos
    expect(axios.get).toHaveBeenCalledWith(
      `${API_URL}/commerces/${commerceId}/platforms`
    );
  });
});
